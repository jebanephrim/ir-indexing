import java.io.*;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/*
 * DataStructure for holding PostingList
 * 
 */
class DSPostingList implements Comparable<DSPostingList>{
	int docid;
	int freq;
	int tsize;
	

	
	int gettsize(){
		return tsize;
	}
	
	void settsize(int totalsize){
		tsize=totalsize;
	}
	
	int getdocid()
	{
		return docid;
	}
	
	int getdocfreq(){
		return freq;
	}
	
	void setdocid(int docId){
		docid=docId;
		
	}
	
	void setdocfreq(int dfreq){
		freq=dfreq;
	}

	@Override
	public int compareTo(DSPostingList o) {
		// TODO Auto-generated method stub
		int comparedVal= o.getdocfreq();
		if(this.getdocfreq() >comparedVal ){
			return 1;
		}
		else if(this.getdocfreq() == comparedVal){
			return 0;
		}
		else{
			return -1;
		}
		
	}
}

/*
 * Class handling Linkedlist operations performed on posting lists stored as posting lists
 * 
 */
class PLinkedList{
	LinkedList<DSPostingList> pList;
	int size;
	
	PLinkedList(){
		pList=new LinkedList<DSPostingList>();
		size=0;
	}
	
	void addPostingtoList(DSPostingList obj){
		pList.add(obj);
	
	}
	
	
	LinkedList<DSPostingList> getPostingListItem(){
		return pList;
	}
	
	int getsizeofpostinglist(){
		return pList.getFirst().gettsize();
		
	}
	void printPostingList(){
		int i;
		for(i=0;i<pList.size();i++)
		System.out.println(pList.get(i).getdocid());
		
	}
	
	
	/*
	 * Function to sort doc sorted by their term frequencies in decreasing order
	 * 
	 */
	
	LinkedList<Integer> display_sorted_freqids(){
		int i;
		LinkedList<DSPostingList> freqlist=new LinkedList<DSPostingList>();
		freqlist=pList;
		LinkedList<Integer> ll= new LinkedList<Integer>();
	
		
		 Collections.sort(freqlist, Collections.reverseOrder());
		
		 for(i=0;i<freqlist.size();i++){
			 if(i==0){
			// System.out.print(freqlist.get(i).getdocid());
			 ll.add(freqlist.get(i).getdocid());
			 }
			 else{
				// System.out.print(","+freqlist.get(i).getdocid());
				 ll.add(freqlist.get(i).getdocid());
			 }
		 }
	
			return ll;	
		}
	
	/*
	 * Function to sort and display docs sorted by their doc ids in increasing order
	 * 
	 */
	
	LinkedList<Integer> display_sorted_docids(){
		int i;
		LinkedList<DSPostingList> doclist=new LinkedList<DSPostingList>();
		LinkedList<Integer> ll= new LinkedList<Integer>();
		// System.out.print("Ordered by doc IDs: ");
		 doclist=pList;
		 Collections.sort(doclist, new Comparator<DSPostingList>() {

			@Override
			public int compare(DSPostingList o1, DSPostingList o2) {
				//int comparedVal= o1.getdocfreq();
				if(o1.getdocid() >o2.getdocid() ){
					return 1;
				}
				else if(o1.getdocid() == o2.getdocid()){
					return 0;
				}
				else{
					return -1;
				}
				
			}
	     });
		
		 for(i=0;i<doclist.size();i++){
			 
			 ll.add(doclist.get(i).getdocid());
			
		 }
		 
				return ll;
		}
	
	
}

public class CSE535Assignment {
	
	static StringBuilder oput = new StringBuilder();
	static LinkedList<Integer> f1;
	static LinkedList<Integer> f2;
	static int comparisons=0;
	static double tdifference=0;
	static int tandcomparisons=0;
	static double tanddifference=0;
	
	
	/*
	 * A simple implementation to check if an element exists in a linkedlist
	 * 
	 */
	static boolean checklist(LinkedList<Integer> list, int element){
		Iterator<Integer> i= list.iterator();
		while(i.hasNext()){
			if(i.next()==element){
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Document at a time or: This function takes a arraylist of Posting lists stored as linkedlists, Each list is 
	 * iterated over simultaneously to find the union of two lists by processing doc at a time.
	 * 
	 */
	
	static LinkedList<Integer> DocumentAtaTimeOr(ArrayList<LinkedList<Integer>> docs,LinkedList<String> query) // DocIDValues- Array of linked lists containing docIDs of query terms
    {
		oput.append("\n\nFUNCTION: docAtATimeQueryOr "+query.toString().replace("[","" ).replace("]","")+" \n");
        int size = docs.size();
        int dcomparisons=0;
        Iterator[] it = new Iterator[size];
        for(int i=0; i<size; i++)
        {
            it[i] = docs.get(i).iterator();
        }
        LinkedList<Integer> answer = new LinkedList<Integer>();
        double lStartTime = new Date().getTime();
        while(true)
        {
            for(int i=0; i<size;i++)
            {
                Integer temp;
                if(it[i].hasNext())
                {
                    temp = (Integer) it[i].next();
                    int flag = 1;
                    for(Iterator iter = answer.iterator();iter.hasNext();)
                    {
                    	dcomparisons++;
                        if(temp.equals(iter.next()))
                        {                     	
                            flag = 0;
                            break;
                        }
                    }

                    if(flag!=0)
                    {
                        answer.add(temp);
                    }
                }
                int canContinue = 0;
                for(int k=0; k<size; k++)
                {
                   if(it[k].hasNext()) 
                   {
                       canContinue = 1;
                       break;
                   }
                }
                if(canContinue==0)
                {
                	 double lEndTime = new Date().getTime();
                     double diff= (lEndTime -lStartTime)/1000;
                     oput.append("\n"+answer.size()+" documents found");
                     oput.append("\n"+dcomparisons+" comparisons made");
                     oput.append("\n"+diff+" seconds used");
                     oput.append("\nResults: ");
                     for(i=0;i<answer.size();i++){
                    if(i==0)
                     {
                    oput.append(answer.get(i))	 ;
                     }
                    else
                    	oput.append(","+answer.get(i));
                     }
                   //  System.out.println("DAAT AndNo of Documents found"+results.size());
                    return answer;
                }
                
          
            }
           
        }
       
    }
	
	/*
	 * 
	 *Document at a time and: lists are simultaneously iterated over to get the intersection of two or more posting lists. 
	 * 
	 */
	static LinkedList<Integer> DocumentAtaTimeAnd(ArrayList<LinkedList<Integer>> docs,LinkedList<String> query) //docValues is an array of linkedlists containing DocIDs
    {   
		oput.append("\n\nFUNCTION: docAtaTimeQueryAnd "+query.toString().replace("[","" ).replace("]","")+"\n");
		int dcomparisons=0;
        int size = docs.size();
       // int size = docValues.length;
        Iterator[] it = new Iterator[size];
        for(int i=0; i<size; i++)
        {
            it[i] = docs.get(i).iterator();
        }
        LinkedList<Integer> answer = new LinkedList<Integer>();

         Integer[] currentTerms = new Integer[size];
         double lStartTime = new Date().getTime();
         for(int k = 0; k<size; k++)
         {   
              currentTerms[k] = (Integer)it[k].next();
         }
         Integer max = currentTerms[0];
         while(true)
         {
             int eqComparisons=0;
             for(int x = 0; x<size; x++)
             {  
                 if(max.compareTo(currentTerms[x])<0)
                 {  
                     dcomparisons++;
                     max = currentTerms[x];
                 }
                 else if(max.compareTo(currentTerms[x])==0)
                 {
                     eqComparisons++;
                     dcomparisons++;
                 }
             }
             int flag = 1;
             if(eqComparisons==size)
             {
                 answer.add(max);
                 for(int j=0;j<size;j++)
                {   
                    if(!it[j].hasNext())
                    {   
                        flag = 0;
                        break;
                    }
                    else
                    {
                        currentTerms[j]=(Integer)it[j].next();
                    }

                 }
                
             }
             else
             {
                for(int j=0;j<size;j++)
                {   
                    
                        if(max.compareTo(currentTerms[j])!=0)
                        {   if(it[j].hasNext())
                            {
                            currentTerms[j] = (Integer)it[j].next();
                            }
                            else
                            {
                                flag = 0;
                                break;
                            }
                          
                        }
                    
                }
             }
             
             if(flag==0)
             {
                 break;
             }
            
         }
        
         //return results;
         double lEndTime = new Date().getTime();
         double diff= (lEndTime -lStartTime)/1000;
         oput.append("\n"+answer.size()+" documents found");
         oput.append("\n"+dcomparisons+" comparisons made");
         oput.append("\n"+diff+" seconds used");
         oput.append("\nResults: ");
         for(int i=0;i<answer.size();i++){
        if(i==0)
         {
        oput.append(answer.get(i))	 ;
         }
        else
        	oput.append(","+answer.get(i));
         }
       //  System.out.println("DAAT AndNo of Documents found"+results.size());
        return answer;
       
    }
	
	/*
	 * a sub routine to perform TAAT OR on posting lists ordered by decreasing freq ids
	 * 
	 */
	
	static LinkedList<Integer> simpleOr(LinkedList<Integer> p1,LinkedList<Integer> p2){
		
		LinkedList<Integer> answer=new LinkedList<Integer>();
		//int comparisons=0;
		int num=0,num1=0,i=0;
	    if(p1.getFirst()==0){
	    	return p2;
	    }
	    else if(p2.getFirst()==0){
	    	return p1;
	    }
		if(p1!=null && p2!=null){
	
		double lStartTime = new Date().getTime();
		for(Iterator<Integer> it1= p1.iterator();it1.hasNext();){
//		while(it1.hasNext() && it2.hasNext()){
			int doc1=it1.next();
			
			for(Iterator<Integer> it2=p2.iterator();it2.hasNext();){	
				
				int doc2=it2.next();
			//System.out.println(doc1 +" "+doc2);
			if(doc1 == doc2  ){
				//System.out.println("not adding to answer");
				//answer.add(doc1);
				comparisons++;
			}
			else
			{
				if(!checklist(answer,doc1))
				answer.add(doc1);
				
				if(!checklist(answer,doc2))
				answer.add(doc2);
				comparisons++;
			}
			/*else if(doc1 < doc2){
				it1.next();
				comparisons++;
			}*/
			
		}
		}
		double lEndTime = new Date().getTime();
		tdifference += (lEndTime - lStartTime)/1000;
		
	}
		return answer;
	}
	
	/*
	 * a sub routine to perform  TAAT AND on posting lists ordered by decreasing Term frequencies
	 * 
	 */
	
	static LinkedList<Integer> simpleAnd(LinkedList<Integer> p1,LinkedList<Integer> p2){
		LinkedList<Integer> answer=new LinkedList<Integer>();
		 if(p1.getFirst()==0){
		    	return answer;
		    }
		    else if(p2.getFirst()==0){
		    	return answer;
		    }
		if(p1!=null && p2!=null){
			//Iterator<DSPostingList> it1= p1.iterator();
			//Iterator<DSPostingList> it2=p2.iterator();
			
			double lStartTime = new Date().getTime();
			for(Iterator<Integer> it1= p1.iterator();it1.hasNext();){
//			while(it1.hasNext() && it2.hasNext()){
				int doc1=it1.next();
				
				for(Iterator<Integer> it2=p2.iterator();it2.hasNext();){		
					int doc2=it2.next();
				//System.out.println(doc1 +" "+doc2);
				if(doc1 == doc2  ){
					//System.out.println("adding to answer");
					answer.add(doc1);
					tandcomparisons++;
				}
				else
					tandcomparisons++;
			
				/*else if(doc1 < doc2){
					it1.next();
					comparisons++;
				}*/
				
			}
			}
			double lEndTime = new Date().getTime();
			 tanddifference += (lEndTime - lStartTime)/1000;
	
		    Collections.sort(answer);
			/*for(int j=0;j<answer.size();j++){
				System.out.print(", "+answer.get(j));
			}*/
			return answer;
			}
			else{
				oput.append("\nPostings not found");
				return null;
			}
		
	}
	
	/*
	 *Performs Term at a time union operation on posting lists iteratively depending on the number of query terms 
	 * 
	 */
	
	static void termAtaTimeQueryOr(ArrayList<LinkedList<Integer>> arr,LinkedList<String> query){
		LinkedList<Integer> answer=new LinkedList<Integer>();
	//	int comparisons=0;
		int num=0,num1=0,i=0;
		oput.append("\n\nFUNCTION: termAtATimeQueryOr "+query.toString().replace("[","" ).replace("]","")+"\n");
		int firsttime=1;
		Iterator<LinkedList<Integer>> ait= arr.iterator();
		LinkedList<Integer> p1= new LinkedList<Integer>();
		LinkedList<Integer> p2= new LinkedList<Integer>();
		//System.out.println("\n\nFUNCTION: termAtATimeQueryAnd ");
		while(ait.hasNext()){
			if(firsttime==0){
				answer= simpleOr(answer,ait.next());
			}else{
			p1=ait.next();
			p2=ait.next();
		answer= simpleOr(p1,p2);
		firsttime=0;
			}
		}
		
		oput.append("\n"+answer.size()+ " documents found");
		oput.append("\n"+comparisons+ " comparisons made");
		oput.append("\n"+tdifference+ " seconds were used");
		oput.append("\nResults: ");
        for(i=0;i<answer.size();i++){
       if(i==0)
        {
       oput.append(answer.get(i))	 ;
        }
       else
       	oput.append(","+answer.get(i));
        }
		
	}
	
	/*
	 * Performs Term at a time intersection operation on posting lists iteratively depending on the number of query terms 
	 * 
	 */
	
static void termAtaTimeQueryAnd(ArrayList<LinkedList<Integer>> arr,LinkedList<String> query){
	LinkedList<Integer> answer=new LinkedList<Integer>();
	//int comparisons=0;
	int q=0;
	int num=0,num1=0,i=0;
	int qSize= arr.size();
	int firsttime=1;
	Iterator<LinkedList<Integer>> ait= arr.iterator();
	LinkedList<Integer> p1= new LinkedList<Integer>();
	LinkedList<Integer> p2= new LinkedList<Integer>();
	oput.append("\n\nFUNCTION: termAtATimeQueryAnd "+query.toString().replace("[","" ).replace("]","")+"\n");
	while(ait.hasNext()){
		if(firsttime==0){
			answer= simpleAnd(answer,ait.next());
		}else{
		p1=ait.next();
		p2=ait.next();
	answer= simpleAnd(p1,p2);
	firsttime=0;
		}
	}
	oput.append("\n"+answer.size()+" documents found");
	oput.append("\n"+tandcomparisons+ " comparisons made");
	oput.append("\n"+tanddifference+ "seconds were used");
	oput.append("\nResults: ");
    for(i=0;i<answer.size();i++){
   if(i==0)
    {
   oput.append(answer.get(i))	 ;
    }
   else
   	oput.append(","+answer.get(i));
    }
}
	/*
	 * 
	 * Get posting lists ordered by doc ids
	 * 
	 */
static LinkedList<Integer> getPostingsByDocId ( LinkedHashMap<String, PLinkedList> lmap,String query){	

	PLinkedList p= new PLinkedList();
	 Set<Entry<String, PLinkedList>> set = lmap.entrySet();
     // Get an iterator
     Iterator<Entry<String, PLinkedList>> it = set.iterator();
     p=lmap.get(query);
    LinkedList<Integer> docbyid=new LinkedList<Integer>();
 	
     int i=0;
     if(p!=null){
     docbyid=p.display_sorted_docids();
    // docbyfreq[i]=p.display_sorted_freqids();
     }
     else{
    	 oput.append("\nTerm not found\n");
    	 docbyid.add(0);
     }
  /*   while(it.hasNext()){
     	Map.Entry<String, PLinkedList> me = (Map.Entry<String, PLinkedList>)it.next();
        if(me.getKey().toString() == query){
        p = me.getValue();
     	
     	break;
        }
        }*/
     return docbyid;

}

/*
 * get posting lists ordered by doc frequencies
 * 
 */

static LinkedList<Integer> getPostingsByDocFreq ( LinkedHashMap<String, PLinkedList> lmap,String query){	

	PLinkedList p= new PLinkedList();
	LinkedList<Integer> docbyfreq=new LinkedList<Integer>();
	 Set<Entry<String, PLinkedList>> set = lmap.entrySet();
     // Get an iterator
     Iterator<Entry<String, PLinkedList>> it = set.iterator();
     p=lmap.get(query);
    // System.out.println("\n\nFUNCTION: getPostings "+query);
     //int i=0;
     if(p!=null){
     //docbyid=p.display_sorted_docids();
     docbyfreq=p.display_sorted_freqids();
     }
     else{
    	oput.append("\nTerm not found");
    	docbyfreq.add(0);
     }
  /*   while(it.hasNext()){
     	Map.Entry<String, PLinkedList> me = (Map.Entry<String, PLinkedList>)it.next();
        if(me.getKey().toString() == query){
        p = me.getValue();
     	
     	break;
        }
        }*/
     return docbyfreq;

}
	

/*
 * GetTopK: This method takes in the entire index as input and build it into a treemap. Then I iterate the collections in reverseorder
 * to get the top K terms in descending order
 */

static void getTopK( LinkedHashMap<String, PLinkedList> lmap,int K ){
	  Set<Entry<String, PLinkedList>> set = lmap.entrySet();
      // Get an iterator
      Iterator<Entry<String, PLinkedList>> it = set.iterator();
      Map< Integer, String> map = new TreeMap<Integer, String>(Collections.reverseOrder());
      while(it.hasNext()){
      	Map.Entry<String, PLinkedList> me = (Map.Entry<String, PLinkedList>)it.next();
       //   System.out.println(me.getKey() + ": "+me.getValue().getsizeofpostinglist() );
          map.put(me.getValue().getsizeofpostinglist(),me.getKey());
          //me.getKey();
      }
     
     oput.append("\nResult: ");
      Set<Entry<Integer, String> >set1 = map.entrySet();
      // Get an iterator
      Iterator<Entry<Integer, String>> i = set1.iterator();
      // Display elements
      int c=0;
      while(i.hasNext()) {
    	  if(c<=K){
         Map.Entry<Integer, String> me = (Map.Entry<Integer, String>)i.next();
       // System.out.print(me.getKey() + ": ");
         if(c==0)
         oput.append(me.getValue());
         else
        	 oput.append("," +me.getValue());
    	  }
    	  else
    		  break;
    	  c++;
      }
	
}


public static void main(String[] args){

	    int postingCount=0;
	    String postingList;
	    String term;
	    int docid;
	    Writer writer = null;
            int i=0;
	    int freq;
	 
        String inputfileName = args[0];
        String outputFile = args[1];
        String K= args[2];
        String queryfilename= args[3];
        String regex= "^(.+)\\\\c([0-9]+)\\\\m(.+)$";
        // This will reference one line at a time
        String line = null;
        LinkedHashMap<String, PLinkedList> lmap = new LinkedHashMap<String, PLinkedList>();
        
        LinkedHashMap<String, LinkedList<DSPostingList>> holder=new LinkedHashMap<String, LinkedList<DSPostingList>>();

        try {
        
            // FileReader reads text files in the default encoding.
	    File f = new File(inputfileName);
            FileReader fileReader = new FileReader(f);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
          //  System.out.println("Getting file...\n");
//Index Construction starts here
            /*
             * 1.parse the text using regex
             * 2. Build a index using Linked hashmap where query term is the key and the value is the corresponding
             * posting list Datastructure.
             * 
             */
            while((line = bufferedReader.readLine()) != null) {

                 Pattern pattern = Pattern.compile(regex);
	         Matcher matcher = pattern.matcher(line);
		PLinkedList plist= new PLinkedList();
		while (matcher.find()){
	              term=matcher.group(1);
	              postingCount=Integer.parseInt(matcher.group(2).trim());
	              postingList=matcher.group(3);	  
	              postingList= postingList.replace('[', ' ');
	              postingList= postingList.replace(']', ' ');
	              String[] pl=postingList.trim().split(", ");
	              DSPostingList[] ds=new DSPostingList[postingCount];

	              for(String v : pl){
	            	  if(i<postingCount){
	            	   ds[i] = new DSPostingList();

	            	 
  				 String[] s=v.split("/");
  				 docid= Integer.parseInt(s[0]);
  				 freq = Integer.parseInt(s[1]);
  				 ds[i].setdocid(docid);
  				 ds[i].setdocfreq(freq);
  				 ds[i].settsize(postingCount);
  				 plist.addPostingtoList(ds[i]);
  			
	            	  }
	            	  i++;
	              }
	              i=0;
	              lmap.put(term,plist);
	            
	              }
            }   
        	FileReader fr= new FileReader(queryfilename);
        	BufferedReader br=new BufferedReader(fr);
        	int cnt=0;
        	LinkedList<LinkedList<String>> qlist= new LinkedList<LinkedList<String>>();
        	while((line=br.readLine())!=null){
        		LinkedList<String> tlist= new LinkedList<String>();
        	String[] q=line.split(" ");
        		for(String str : q){
        			tlist.add(str);
        			cnt ++;
        		}
        		qlist.add(tlist);
        	}
        	 oput.append("FUNCTION: getTopK "+ K+"\n");
        	//GetTopK is executed only once.
            getTopK(lmap,Integer.parseInt(K));
            
            ArrayList<LinkedList<Integer>> arr_bydocid= new ArrayList <LinkedList<Integer>>();
            ArrayList<LinkedList<Integer>> arr_byfreqid= new ArrayList <LinkedList<Integer>>();
            int c=0;
            int d=0;
            //The loop where the query terms are read from file line by line and perform TAAT and DAAT operations
            for(c=0;c < qlist.size();c++){
            	for(d=0;d< qlist.get(c).size();d++){          		
            		 LinkedList<Integer> by_id = new LinkedList<Integer>();
            		 LinkedList<Integer> by_freq = new LinkedList<Integer>();
            		 oput.append("\n\nFUNCTION: getPostings "+qlist.get(c).get(d).toString()+"\n");
            		 by_id=getPostingsByDocId(lmap,qlist.get(c).get(d).toString());
            		 oput.append("Ordered by doc : ");
            		 for(i=0;i<by_id.size();i++){
            			 if(i==0){
            			 oput.append(by_id.get(i));
            			 }
            			 else{
            				 oput.append(","+by_id.get(i));
            			 }
            		 }
            		 oput.append("\nOrdered by TF: ");
            		 by_freq=getPostingsByDocFreq(lmap,qlist.get(c).get(d).toString());
            		 for(i=0;i<by_freq.size();i++){
            			 if(i==0){
            			 oput.append(by_freq.get(i));
            			 }
            			 else{
            				 oput.append(","+by_freq.get(i));
            			 }
            		 }
            		 arr_bydocid.add(by_id);
            		 arr_byfreqid.add(by_freq);
            	}
            	
            	termAtaTimeQueryAnd(arr_byfreqid,qlist.get(c));
            	termAtaTimeQueryOr(arr_byfreqid,qlist.get(c));
            	DocumentAtaTimeAnd(arr_bydocid,qlist.get(c));
            	DocumentAtaTimeOr(arr_bydocid,qlist.get(c));
            bufferedReader.close();  
            }
            
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" +inputfileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '"+ inputfileName + "'");                  
}
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                  new FileOutputStream(outputFile), "utf-8"));
            writer.write(oput.toString());
        } catch (IOException ex) {
          // report
        } finally {
           try {writer.close();} catch (Exception ex) {/*ignore*/}
        }       
}

}



